import React from 'react';

export default class NavigationBar extends React.Component {
    constructor(props) {
        super(props);

        this.redirectTo = this.redirectTo.bind(this);
    }

    redirectTo(path) {
        console.log(this.props.history);
        this.props.history.push(path);
    }
    
    render() {
		return (
            <div id="NavBarHome">
                <button onClick={() => {this.redirectTo("/formations")}}></button>
                <button onClick={() => {this.redirectTo("/professionals_experiences")}}></button>
                <button onClick={() => {this.redirectTo("/professionals_skills")}}></button>
                <button onClick={() => {this.redirectTo("/hobbies")}}></button>
            </div>
		);
	}
}