import React from 'react';

export default class Description extends React.Component {
    render() {
		return (
            <div id="DescriptionHome">
                <h1>Bastien Lécussan</h1>
                <p>Hello, my name is Bastien Lecussan and I am actually an Epitech student.</p>
                <p>The wide universe of the computer sciences is something i discovered not a long time ago... and I loved it immediatly.</p>
                <p>If youre here, it's because you want to know more about me, so let's talk!</p>
            </div>
		);
	}
}