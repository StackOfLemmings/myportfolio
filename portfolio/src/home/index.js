import React from 'react';
import NavBar from './nav_bar';
import Description from './description';

export default class Home extends React.Component {
    render() {
		return (
            <div id="Home">
                <Description/>
                <NavBar {...this.props}/>
            </div>
		);
	}
}