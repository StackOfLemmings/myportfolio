import React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import Home from "./home";
import Formation from "./formation";
import ProfessionalSkills from './professionalSkills';
import ProfessionalExperiences from './professionalExperiences';
import Hobbies from "./hobbies";

export default class Routes extends React.Component {
	render() {
		return (
		<div>
			<Router>
				<Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/formations" component={Formation}/>
                    <Route exact path="/professionals_experiences" component={ProfessionalExperiences}/>
                    <Route exact path="/professionals_skills" component={ProfessionalSkills}/>
                    <Route exact path="/hobbies" component={Hobbies}/>
					<RouteDefault/>
				</Switch>
			</Router>
		</div>
		);
	}
};

const RouteDefault = ({ component: Component, ...rest }) => (
	<Route {...rest} render={props =>
		<Redirect to={{pathname: "/", state: { from: props.location }}}/>
	}/>
);